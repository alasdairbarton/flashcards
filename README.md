[![Netlify Status](https://api.netlify.com/api/v1/badges/061d74b5-ee68-4f54-99dd-662793065c01/deploy-status)](https://app.netlify.com/sites/vigorous-newton-bcc6bd/deploys)
# Welcome 👋 to Alasdair Barton's flashcard app 🗃️

🔴 View live build here: <https://flashcards.projects.alasdairbarton.design/>

This is to make up for all the computer science lessons where I just sit there and do nothing ;)
Also I need to learn [vue.js](https://v3.vuejs.org/). Hosted on [Netlify](https://www.netlify.com/), source code available on here.

*Probably some of the worst code I've written in ages. Oh well, it looks nice I guess ¯\_(ツ)_/¯*

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```
### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
