import { createRouter, createWebHistory } from 'vue-router'
// import { createRouter } from 'vue-router'
// import App from '@/App.vue'
import Home from '@/views/Home.vue'
// import Learn from '@/views/Learn.vue'
// import Create from '@/views/Create.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/learn',
    name: 'Learn',
    component: () => import('@/views/Learn.vue')
    // component: Learn
  },
  {
    path: '/create',
    name: 'Create',
    component: () => import('@/views/Create.vue')
    // component: Create
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

export default router

